// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedUpFoodBase.h"
#include "SnakeActorBase.h"

// Sets default values
ASpeedUpFoodBase::ASpeedUpFoodBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpeedUpFoodBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedUpFoodBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedUpFoodBase::Interact(AActor* interactor, bool bIsHead)
{
	ASnakeActorBase* snake = Cast<ASnakeActorBase>(interactor);
	if (IsValid(snake)) {
		
		snake->SetActorTickInterval(snake->GetActorTickInterval() - 0.05);
		this->Destroy();
	}
}

