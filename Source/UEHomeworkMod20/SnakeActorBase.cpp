// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeActorBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeActorBase::ASnakeActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 150;
	Speed = 1;
	LastDirection = EMovementDirection::DOWN;
	bCanAcceptInput = true;

}

// Called when the game starts or when spawned
void ASnakeActorBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElements(4);
	SetActorTickInterval(Speed);
	
}

// Called every frame
void ASnakeActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	bCanAcceptInput = true;

}

void ASnakeActorBase::AddSnakeElements(int elementsNum)
{
	for (int i = 0; i < elementsNum; i++) {
		FVector newLocation(SnakeElementsArr.Num() * ElementSize, 0, 60);
		FTransform newTransform(newLocation);
		ASnakeElementBase* element = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, newTransform);
		element->Mesh->SetVisibility(false);
		element->SnakeOwner = this;
		int index = SnakeElementsArr.Add(element);
		if (index == 0) {
			element->SetFirstElementType();
		}
	}
}

void ASnakeActorBase::Move()
{
	FVector direction(ForceInitToZero);

	
	switch (LastDirection) {
	case EMovementDirection::UP:
		direction.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		direction.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		direction.Y -= ElementSize;
		break;
	case EMovementDirection::RIGHT:
		direction.Y += ElementSize;
		break;
	default:
		direction.X -= ElementSize;
		break;
		
	}

	SnakeElementsArr[0]->ToogleCollision();

	for (int i = SnakeElementsArr.Num() - 1; i > 0; i--) {
		auto currentElement = SnakeElementsArr[i];
		auto prevElement = SnakeElementsArr[i - 1];
		currentElement->SetActorLocation(prevElement->GetActorLocation());
		currentElement->Mesh->SetVisibility(true);

	}

	SnakeElementsArr[0]->AddActorWorldOffset(direction);
	SnakeElementsArr[0]->ToogleCollision();
	SnakeElementsArr[0]->Mesh->SetVisibility(true);
}

void ASnakeActorBase::SnakeElementOverlap(ASnakeElementBase* OverlapedBlock, AActor* Other)
{
	int index;
	SnakeElementsArr.Find(OverlapedBlock, index);
	bool bIsFirst = index == 0;
	IInteractable* interactableInterface = Cast<IInteractable>(Other);
	
	if (interactableInterface) {
		interactableInterface->Interact(this, bIsFirst);
	}
}

void ASnakeActorBase::DestroySnake()
{
	for (auto& element : SnakeElementsArr) {
		element->Destroy();

	}
	this->Destroy();
}

void ASnakeActorBase::SetDirection(EMovementDirection Direction)
{
	if (!bCanAcceptInput) return;
	LastDirection = Direction;
	bCanAcceptInput = false;
}

EMovementDirection ASnakeActorBase::GetDirection()
{
	return LastDirection;
}



